﻿using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using SY_Anekay_LMJ_OutPutExcelReport;
using Kingdee.BOS.Core.DynamicForm;
using Kingdee.BOS.Util;
using Kingdee.BOS.Core;

namespace SY_Anekay_LMJ
{
    public class ExcelReportFormPlugin:AbstractDynamicFormPlugIn
    {
        public override void AfterBindData(EventArgs e)
        {
            base.AfterBindData(e);
            using(IDataReader reader=DBServiceHelper.ExecuteReader(this.Context, "Select F_SY_WORKINGORDERNO from SY_T_DISHESBILL"))
            {
                while(reader.Read())
                {
                    string WO = Convert.ToString(reader["F_SY_WORKINGORDERNO"]);
                    Model.CreateNewEntryRow("F_SY_Entity");
                    Model.SetValue("F_SY_WorkOrder",WO,Model.GetEntryCurrentRowIndex("F_SY_Entity"));
                }
                View.UpdateView("F_SY_Entity");
            }
        }


        public override void EntryCellFocued(EntryCellFocuedEventArgs e)
        {
            base.EntryCellFocued(e);
            Model.SetValue("F_SY_CurrentWO", Convert.ToString(Model.GetValue("F_SY_WorkOrder", e.NewRow)));
            View.UpdateView("F_SY_CurrentWO");
        }
        
        public override void AfterButtonClick(AfterButtonClickEventArgs e)
        {
            base.AfterButtonClick(e);
            string workOrder = Convert.ToString(Model.GetValue("F_SY_CurrentWO"));
            string before = Convert.ToString(Model.GetValue("F_SY_FlashBefore"));
            string after = Convert.ToString(Model.GetValue("F_SY_FlashAfter"));

            if (e.Key == "F_SY_Release".ToUpper())
            {
                CreateExcelReportDownloadLink(workOrder,null,null,before,after);
            }
            if (e.Key== "F_SY_ReleaseDaliy".ToUpper())
            {
                var start= Model.GetValue("F_SY_StartDate") as DateTime?;
                var end = Model.GetValue("F_SY_EndDate") as DateTime?;
                if (start == null || end == null)
                    this.View.ShowErrMessage("日报表请输入日期");
                CreateExcelReportDownloadLink(workOrder, start, end,before,after);
            }


        }

        private void CreateExcelReportDownloadLink(string workOrder,DateTime? start,DateTime? end,string flashBefore,string flashAfter)
        {
            OutPutExcelReportTool tool = new OutPutExcelReportTool();
            DataSet dataSet = new DataSet();
            if (start == null && end == null)
            {
                //结案从视图取数
                //Views Count:5                
                dataSet = DBServiceHelper.ExecuteDataSet(this.Context, $@"
Select top 1 * from V_SY_BINHeadData where WorkOrder = '{workOrder}' ;
Select top 1 * from V_SY_BINSubTitle where WorkOrder='{workOrder}';
Select * from V_SY_BINEntryData where WorkOrder='{workOrder}';
SELECT top 1 * FROM v_sy_capacityqty WHERE workorder='{workOrder}';
SELECT * FROM V_SY_ExamineSummaryEntry where WorkOrder='{workOrder}'");
            }
            else
            {
                //日报从存储过程取数，取CapacityQty、SummaryEntry
                //Views Count:3,procedure:2
                dataSet = DBServiceHelper.ExecuteDataSet(this.Context, $@"
Select top 1 * from V_SY_BINHeadData where WorkOrder = '{workOrder}' ;
Select top 1 * from V_SY_BINSubTitle where WorkOrder='{workOrder}';
Select * from V_SY_BINEntryData where WorkOrder='{workOrder}'AND DateTime>='{start}' AND DateTime<='{end}';
exec [proc_sy_CapacityQTY] '{workOrder}','{start}','{end}';
exec proc_SY_ExamineSummaryEntry '{workOrder}','{start}','{end}'");
                
            }

            dataSet.Tables[0].TableName = "BINHeadData";
            dataSet.Tables[1].TableName = "BINSubTitle";
            dataSet.Tables[2].TableName = "BINEntryData";
            dataSet.Tables[3].TableName = "CapacityQty";
            dataSet.Tables[4].TableName = "SummaryEntry";
            tool.DataSet = dataSet;

            tool.WorkOrder = workOrder;
            tool.InitialWoroBook();
            if (start == null && end == null)
                tool.LSYMethod(DateTime.MinValue, DateTime.MaxValue);
            else
                tool.LSYMethod( (DateTime)start,(DateTime)end);
            tool.CreateWorkReport(flashBefore,flashAfter);

            string filename = tool.OutPut(@"C:\Program Files (x86)\Kingdee\K3Cloud\WebSite\tempfilePath\");


            var param = new DynamicFormShowParameter();
            param.FormId = "BOS_FileDownLoad";
            param.OpenStyle.ShowType = ShowType.Modal;
            param.CustomParams.Add("IsExportData", "true");
            param.CustomParams.Add("url", "TempfilePath/" + filename);
            this.View.ShowForm(param);
        }
    }
}
