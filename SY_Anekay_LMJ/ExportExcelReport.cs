﻿using Kingdee.BOS.Core;
using Kingdee.BOS.Core.DynamicForm;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.ServiceHelper.Excel;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace SY_Anekay_LMJ
{
    [Description("动态表单插件-导出Excel文件")]
    public class ExportExcelReport : AbstractDynamicFormPlugIn
    {
        
        public override void BarItemClick(BarItemClickEventArgs e)
        {
            //base.BarItemClick(e);
            if (e.BarItemKey == "tbExportExcel")
            {
                this.ExportExcel();
            }
        }

        private void ExportExcel()
        {
            var entity = this.View.BusinessInfo.GetEntity("FEntity");
            var curRows = this.View.Model.GetEntityDataObject(entity);
            if (curRows.Count == 0)
            {
                this.View.ShowMessage("无可导出数据！");
                return;
            }

            DataSet dataSet = new DataSet();
            DataTable dt = dataSet.Tables.Add(this.Model.BusinessInfo.GetForm().Id);
            List<string> outFieldKes = new List<string>() { "F_bos_Text" };//需要导出的字段Key标识
            Dictionary<string, string> Fields = new Dictionary<string, string>();
            foreach (var field in entity.Fields)
            {
                if (outFieldKes.Contains(field.PropertyName))
                {
                    dt.Columns.Add(field.PropertyName);//添加列
                    Fields.Add(field.PropertyName, field.Name);//
                }
            }

            dt.BeginInit();
            DataRow drKey = dt.NewRow();
            DataRow drName = dt.NewRow();
            foreach (var field in Fields)
            {
                drKey[field.Key] = field.Key;
                drName[field.Key] = field.Value;
            }
            dt.Rows.Add(drKey);//Add Key行
            dt.Rows.Add(drName);//Add Name行

            foreach (var item in curRows)//循环需要导出的数据
            {
                DataRow dr = dt.NewRow();
                foreach (var field in Fields)
                {
                    dr[field.Key] = item[field.Key];//赋值
                }
                dt.Rows.Add(dr);
            }
            dt.EndInit();

            ExcelOperation helper = new ExcelOperation(this.View);
            helper.BeginExport();
            helper.ExportToFile(dataSet);

            string fileName = string.Format("{0}_{1}", this.Model.BusinessInfo.GetForm().Name,
                               DateTime.Now.ToString("yyyyMMddHHmmssff"));

            string[] IllegalStrs = new string[] { "/", "\" };
            foreach (var str in IllegalStrs)
            {
                fileName = fileName.Replace(str, "");
            }

            fileName = PathUtils.GetValidFileName(fileName);
            string _FilePath = PathUtils.GetPhysicalPath(KeyConst.TEMPFILEPATH, fileName);
            string _OutServicePath = PathUtils.GetServerPath(KeyConst.TEMPFILEPATH, fileName);
            _FilePath += ".xls";
            _OutServicePath += ".xls";
            //生成Excel文件
            helper.EndExport(_FilePath, SaveFileType.XLS);

            //下载文件
            DynamicFormShowParameter param = new DynamicFormShowParameter();
            param.FormId = "BOS_FileDownLoad";
            param.OpenStyle.ShowType = ShowType.Modal;
            param.CustomParams.Add("IsExportData", "true");
            param.CustomParams.Add("url", _OutServicePath);

            this.View.ShowForm(param);
        }
    }
}