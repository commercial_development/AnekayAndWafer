﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SY_Anekay_LMJ_OutPutExcelReport;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        OutPutExcelReportTool tool;
        DataSet data;

        

        public void Initial()
        {
            tool = new OutPutExcelReportTool("192.168.1.231", "ANK_AIS20180129", "sa", "qwe123", "LQ180309-FFK8-16G01");
            data = tool.ConnectDatabase("WW1803061446-FFK8");
        }

        [TestMethod]
        public void OutPutProductivity()
        {
            Initial();
            SqlConnection sqlConnection = new SqlConnection(tool.ConnectString);
            sqlConnection.Open();
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM dbo.V_SY_Productivity", sqlConnection);
            data = new DataSet();
            adapter.Fill(data);
            data.Tables[0].TableName = "V_SY_Productivity";
            tool.DataSet = data;
            tool.InitialWoroBook();
            tool.CreateProductivity(4,4,2018);
            tool.OutPut(@"E:\");
        }

       

        [TestMethod]
        public void VirtualData()
        {
            Initial();
            tool.ConnectDatabaseForTest("VR180410-16GA");
            tool.WorkOrder = "VR180410-16GA";
            tool.InitialWoroBook();            
            tool.LSYMethod(DateTime.MinValue,DateTime.MaxValue);
            tool.CreateWorkReport();
            tool.OutPut(@"F:\");
        }


    }
}
